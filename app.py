#!/usr/bin/python
# -*- coding: <encoding name> -*-

import sqlite3
import socket
import ipaddress
from netaddr import *
from flask import Flask, render_template, request, url_for, flash, redirect
from werkzeug.exceptions import abort
from netmiko import ConnectHandler
from datetime import datetime

username = 'cisco'
password = 'cisco'

ip = '192.168.1.10'

device = {
	'username' : username ,
	'password' : password ,
	'device_type' : 'cisco_ios' ,
	'ip' : ip,
}

def get_db_connection():
    conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    return conn

def get_client(client_id):
    conn = get_db_connection()
    client = conn.execute('SELECT * FROM clients WHERE id = ?',
                        (client_id,)).fetchone()
    conn.close()
    if client is None:
        abort(404)
    return client

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret'

@app.route('/')
def index():
    conn = get_db_connection()
    clients = conn.execute('SELECT * FROM clients').fetchall()
    conn.close()
    return render_template('index.html', clients=clients)

@app.route('/<int:client_id>')
def client(client_id):
    client = get_client(client_id)
    return render_template('client.html', client=client)

@app.route('/create', methods=('GET', 'POST'))
def create():
    if request.method == 'POST':
        name = request.form['name']

        if not name:
            flash('Name is required!')
	else:
            conn = get_db_connection()
            conn.execute('INSERT INTO clients (name) VALUES (?)',(name,))
            conn.commit()
            conn.close()

        return redirect(url_for('index'))

    return render_template('create.html')

@app.route('/<int:id>/config', methods=('GET','POST'))
def config(id):
    client = get_client(id)

    if request.method == 'POST':
        name = request.form['name']
        ip = request.form['ip']
        gw = request.form['gw']

        if not name:
            flash('Name is required!')
        elif IPAddress(gw) not in IPNetwork(ip):
            flash('Gateway not in the same network!')
	elif IPNetwork(ip) not in IPNetwork('164.166.0.0/16'):
	        flash('Network not in 164.166.0.0/16!')
        else:
            conn = get_db_connection()
            conn.execute('UPDATE clients SET name = ?, ip = ?, gw = ?'
                         ' WHERE id = ?',
                         (name, ip, gw, id))
            conn.commit()
            conn.close()

#            config_commands = ['ip vrf'+name,'rd 65556:'+id,'route-target both 65556:'+id,'int ge0/0','int ge0/0.'+id,
#            'ip vrf forwarding'+name,'ip address'+ip+'255.255.255.240','router ospf'+id+'vrf'+name,'router-id'+ip,
#            'interface ge0.0.'+id,'ip ospf'+id+'area 0','router bgp 65556','address-family ipv4 vrf'+name,
#            'redistribute ospf'+id,'router ospf'+id,'redistribute bgp 65556 subnets','do write',
#            'copy running-config ftp://cisco:cisco@FTPServer/backup/'+date+'router1.txt']
#            device = ConnectHandler(device)
#            device.send_config_set(config_commands)
#            device.disconnect()

        return redirect(url_for('index'))

    return render_template('config.html', client=client)

@app.route('/<int:id>/delete', methods=('POST',))
def delete(id):
    client = get_client(id)
    conn = get_db_connection()
    conn.execute('DELETE FROM clients WHERE id = ?', (id,))
    conn.commit()
    conn.close()

#    config_commands = ['no router ospf'+id,'no address-family ipv4 vrf'+name,'no ip ospf'+id+'area 0','no interface ge0.0.'+id,
#    'no router-id'+ip,'no router-target both 65556:'+id,'no ip vrf'+name,'do write',
#    'copy running-config ftp://cisco:cisco@FTPServer/backup/'+date+'router1.txt']
#    device = ConnectHandler(device)
#    device.send_config_set(config_commands)
#    device.disconnect()

    flash('"{}" was successfully deleted!'.format(client['name']))
    return redirect(url_for('index'))

app.run(host='10.251.43.74', port=8080)