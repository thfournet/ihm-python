import sqlite3

connection = sqlite3.connect('database.db')

with open('schema.sql') as f:
    connection.executescript(f.read())

cur = connection.cursor()

cur.execute("INSERT INTO clients (name, ip, gw) VALUES (?, ?, ?)",
            ('Thomas', '164.166.0.0/28', '164.166.0.1')
            )

cur.execute("INSERT INTO clients (name, ip, gw) VALUES (?, ?, ?)",
            ('Anthony', '164.166.0.16/28', '164.166.0.17')
            )

connection.commit()
connection.close()
